(set-env!
 :source-paths   #{"src/clj"}
 :dependencies   '[[compojure                 "1.6.0"]
                   [environ                   "1.1.0"]
                   [hiccup                    "1.0.5"]
                   [pandeiro/boot-http        "0.8.3"    :scope "test"]
                   [ring/ring-core            "1.6.3"]
                   [ring/ring-devel           "1.6.3"]
                   [ring/ring-jetty-adapter   "1.6.3"]
                   [ring/ring-servlet         "1.6.3"]
                   [org.clojure/clojure       "1.9.0"    :scope "provided"]
                   [org.clojure/java.jdbc     "0.7.3"]
                   [org.postgresql/postgresql "42.1.4"]])


(require
 '[pandeiro.boot-http :as http])


(def +project+ 'manenko/planningpoker)
(def +version+ "0.0.1-SNAPSHOT")


(task-options!
 pom  {:project     +project+
       :version     +version+
       :description "Planning poker application for agile teams"}
 aot  {:namespace   '#{planningpoker.core}}
 jar  {:main        'planningpoker.core
       :file        "app.jar"}
 sift {:include     #{#"app.jar"}})


(deftask run
  []
  (comp
   (http/serve
    :handler 'planningpoker.core/app
    :reload  true)
   (watch)
   (target)))


(deftask build
  []
  (comp
   (aot)
   (pom)
   (uber)
   (jar)
   (sift)
   (target)))
