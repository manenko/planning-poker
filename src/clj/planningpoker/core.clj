(ns planningpoker.core
  (:gen-class)
  (:require
   [compojure.core     :refer [defroutes GET]]
   [compojure.route    :refer [not-found]]
   [environ.core       :refer [env]]
   [ring.adapter.jetty :refer [run-jetty]]
   [hiccup.page        :refer [html5]]
   [hiccup.core        :refer [html h]]))


(defn page
  [{:keys [title
           content]
    :or   {title   "Planning Poker"
           content (html [:span])}}]
  (html5 {:lang  :en
          :class :no-js}
         [:head
          [:meta   {:charset     :utf-8}]
          [:meta   {:http-equiv  :x-ua-compatible
                    :content     "ie=edge"}]
          [:meta   {:name        :viewport
                    :content     "width=device-width, initial-scale=1.0"}]
          [:title title]
          [:link {:rel         "stylesheet"
                  :href        "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/css/foundation.min.css"
                  :integrity   "sha256-GSio8qamaXapM8Fq9JYdGNTvk/dgs+cMLgPeevOYEx0="
                  :crossorigin :anonymous}]]
         [:body
          [:div.grid-container
           [:div.grid-x.grid-padding-x
            [:div.large-12.cell
             [:h1 "Welcome to Planning Poker"]]]
           [:div.grid-x
            content]]
          [:script {:src         "https://code.jquery.com/jquery-3.2.1.min.js"
                    :integrity   "sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                    :crossorigin :anonymous}]
          [:script {:src         "https://cdnjs.cloudflare.com/ajax/libs/what-input/5.0.3/what-input.js"
                    :integrity   "sha256-T/sySdE4x1bvwk+oJAi2IPo19amYMLrxpdoJi+3OAoE="
                    :crossorigin :anonymous}]
          [:script {:src         "https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/js/foundation.min.js"
                    :integrity   "sha256-mRYlCu5EG+ouD07WxLF8v4ZAZYCA6WrmdIXyn1Bv9Vk="
                    :crossorigin :anonymous}]
          [:script "$(document).foundation();"]]))


(defn index
  [request]
  {:status  200
   :body    (page {:content (html
                             [:button.button {:type :button} "Skål!"])})
   :headers {"Content-Type" "text/html"}})


(defroutes app
  (GET "/" [] index)
  (not-found "Not Found"))


(defn -main
  [& [port]]
  (let [port (Integer. (or port (env :port) 3000))]
    (run-jetty)))
